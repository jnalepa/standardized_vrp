# Standardized Validation of Vehicle Routing Algorithms

**Tomasz Jastrzab, Michal Myller, Lukasz Tulczyjew, Miroslaw Blocho, Michal Kawulok, Adam Czornik, and Jakub Nalepa**

This repository contains (1) the maps of pickup and delivery points, tests and example solutions of the benchmark set presented in the above-mentioned paper, (2) supplementary material including the detailed experimental results, (3) the detailed data description, and (4) the video presenting our framework.
